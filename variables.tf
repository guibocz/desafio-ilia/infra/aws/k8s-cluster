##############
### GITLAB ###
##############

variable "gitlab_api_token" {
  type = string
}

###################
### AWS GENERAL ###
###################

variable "aws_region" {
  type = string
}

###########
### EKS ###
###########

variable "cluster_name" {
  type = string
}