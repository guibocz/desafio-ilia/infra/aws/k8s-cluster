terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.9.0"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "3.13.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.10.0"
    }
    null = {
      source = "hashicorp/null"
    }
  }
  required_version = ">= 1.1.4"
}

provider "aws" {
  region = var.aws_region
}

data "aws_availability_zones" "available" {}

provider "gitlab" {
  base_url = "https://gitlab.com/api/v4/"
  token    = var.gitlab_api_token
}

provider "kubernetes" {
  config_path = "~/.kube/config"
}