# EKS

This project is based on: https://github.com/hashicorp/terraform-provider-aws/tree/main/examples/eks-getting-started

## Purpose

This project deploys a K8S cluster on AWS using EKS, with a single EC2 worker node.

To meet the minimum requirements to run the ELK stack, the worker node is a t3.xlarge instance. 

## Dependencies

The CI of this project depends on the following environment variables that should be defined in GitLab:

| Name                    | Value                                     |
|-------------------------|-------------------------------------------|
| AWS_ACCESS_KEY_ID       | access key to aws account                 |
| AWS_SECRET_ACCESS_KEY   | secret key to aws account                 |
| TF_VAR_gitlab_api_token | token to the gitlab api                   |
| S3_BUCKET               | name of the bucket on aws (for tfstate)   |
| S3_REGION               | region of the bucket on aws (for tfstate) |