resource "gitlab_group_variable" "kubeconfig_gitlab" {
  group         = 51814282
  key           = "kubeconfig_gitlab"
  value         = local.kubeconfig
  variable_type = "file"
  protected     = false
  # lifecycle {
  #   prevent_destroy = true
  # }
}
